package main

import (
	"fmt"
	"strconv"

	"gopkg.in/telegram-bot-api.v4"
)

func Start(upd tgbotapi.Update) string {
	chatID := upd.Message.Chat.ID
	var err error
	if !chats.Find(chatID) {
		err = chats.Add(chatID)
	}

	if err != nil {
		return "Привет. Что-то пошло не так. Попробуй стартовать меня чуть позже:("
	}

	return "Привет! Я буду присылать тебе цитаты с баша."
}

func Stop(upd tgbotapi.Update) string {
	chatID := upd.Message.Chat.ID

	if chats.Find(chatID) {
		chats.Delete(chatID)
	}

	return "Возвращайся!"
}

func SetMinRate(upd tgbotapi.Update) string {
	chatID := upd.Message.Chat.ID
	rateS := upd.Message.CommandArguments()

	rate, err := strconv.Atoi(rateS)
	if err != nil {
		return "Это не число, чувак. Введи что-нибудь нормальное"
	}

	u := chats.GetUser(chatID)
	u.MinRate = rate

	return "Ok!"
}

func AutoOnOff(upd tgbotapi.Update) string {
	chatID := upd.Message.Chat.ID
	u := chats.GetUser(chatID)
	if u.AutoUpdate {
		u.AutoUpdate = false
		chats.DelTask(u)
		return "autoQuote mod off"
	}
	u.AutoUpdate = true
	chats.AddTask(u)
	return "autoQuote mod on. " + fmt.Sprintf("Ok! Теперь цитаты будут присылаться каждые [%v] минут", u.UpdatePerTime)
}

func GetHelpInfo() string {
	return `
	/getquotes - высылает несколько цитат
	/setminrate - выставить минимальный рейтинг цитаты
	/automod - включить/выключить автоприсылание цитат раз в какое-то время(по умолчанию, 1 час)
	/setautotimeout - выставить промежуток времени для автомода(в минутах)`
}

func SetAutoTimeout(upd tgbotapi.Update) string {
	chatID := upd.Message.Chat.ID
	u := chats.GetUser(chatID)
	t := upd.Message.CommandArguments()
	tinm, err := strconv.Atoi(t)
	if err != nil {
		return "Не смог распознать время! Введите по-другому"
	}
	u.UpdatePerTime = tinm
	chats.AddTask(u)
	return fmt.Sprintf("Ok! Теперь цитаты будут присылаться каждые [%v] минут", u.UpdatePerTime)
}
