package main

import (
	"fmt"
)

type Quote struct {
	Text   string
	Rate   int
	Number int
}

func (q Quote) String() string {
	return fmt.Sprintf("#%d;;;Rate:%d\n%s", q.Number, q.Rate, q.Text)
}
