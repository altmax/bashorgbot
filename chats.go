package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"time"

	"gopkg.in/telegram-bot-api.v4"
)

var (
	saveTime    = 1 * time.Minute
	taskTime    = 1 * time.Minute
	randomQuote = ""
)

type Chats struct {
	Bot         *tgbotapi.BotAPI
	Users       map[int64]*User `json:"users"`
	Tasks       map[int64]*Task `json:"tasks"`
	RandomQuote string          `json:"randomQuote"`
}

type Task struct {
	ChatID int64
	Time   time.Time
}

func (c *Chats) Load() {
	file, err := os.Open("users.json")
	if err != nil {
		log.Panic(err)
	}

	defer file.Close()

	dec := json.NewDecoder(file)
	dec.Decode(c)
}

func (c *Chats) Find(chatID int64) bool {
	_, ok := c.Users[chatID]
	return ok
}

func (c *Chats) Add(chatID int64) error {
	c.Users[chatID] = NewUser(chatID)
	return c.save()
}

func (c *Chats) Delete(chatID int64) error {
	delete(c.Users, chatID)
	_, ok := c.Users[chatID]
	if !ok {
		return c.save()
	}
	return errors.New("some_error")
}

func (c *Chats) save() error {
	b, _ := json.Marshal(c)
	err := ioutil.WriteFile("users.json", b, 777)

	return err
}

func (c *Chats) GetUser(chatID int64) *User {
	if !c.Find(chatID) {
		if err := c.Add(chatID); err != nil {
			return nil
		}
	}

	return c.Users[chatID]
}

func (c *Chats) ParallelSave() {
	for {
		time.Sleep(saveTime)
		c.save()
	}
}

func (c *Chats) TaskSend() {
	for {
		// log.Println("***************", "task send in for")
		time.Sleep(taskTime)
		now := time.Now()
		for _, v := range c.Tasks {
			// log.Println("***************", "task send in for for", now.Sub(now.Add(2*time.Minute)))
			if now.Sub(v.Time).Seconds() > 0 {
				c.UpdateRandomQuote()
				c.Send(v.ChatID, c.RandomQuote)
				v.Time = now.Add(time.Duration(c.GetUser(v.ChatID).UpdatePerTime) * time.Minute)
			}
		}

	}
}

func (c *Chats) Send(chatID int64, text string) {
	msg := tgbotapi.NewMessage(chatID, text)
	c.Bot.Send(msg)
}

func (c *Chats) UpdateRandomQuote() {
	// log.Println("***************", "update random quote")
	qs := GetQuotes(tgbotapi.Update{UpdateID: -1})
	// log.Println("***************", "update random quote", len(qs))
	c.RandomQuote = qs[rand.Intn(len(qs))]
	// log.Println("***************", randomQuote)
}

func (c *Chats) AddTask(u *User) {
	// log.Println("***************", "add task")
	c.Tasks[u.ID] = &Task{ChatID: u.ID, Time: time.Now().Add(time.Duration(u.UpdatePerTime) * time.Minute)}
}

func (c *Chats) DelTask(u *User) {
	// log.Println("***************", "del task")
	delete(c.Tasks, u.ID)
}
