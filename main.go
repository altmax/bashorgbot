package main

import (
	"html"
	"log"

	"gopkg.in/telegram-bot-api.v4"
)

var (
	chats   = Chats{Tasks: map[int64]*Task{}, Users: map[int64]*User{}}
	buttons = [][]tgbotapi.KeyboardButton{
		[]tgbotapi.KeyboardButton{
			tgbotapi.KeyboardButton{Text: "/getquotes"},
			tgbotapi.KeyboardButton{Text: "/automod"},
		},
	}
	keyboard = tgbotapi.ReplyKeyboardMarkup{
		ResizeKeyboard: true,
		Keyboard:       buttons,
	}
)

func main() {
	chats.Load()
	go chats.ParallelSave()
	go chats.TaskSend()
	bot, err := tgbotapi.NewBotAPI("245802974:AAFokx3nu77CA602jmPYtASwKUEileKTfOM")
	if err != nil {
		log.Panic(err)
	}
	chats.Bot = bot
	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)

	u.Timeout = 60
	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil {
			continue
		}

		chatID := update.Message.Chat.ID

		answer := ""
		large := false
		largeAnswer := make([]string, 0)

		if update.Message.IsCommand() {
			switch update.Message.Command() {
			case "start":
				answer = Start(update)
			case "stop":
				answer = Stop(update)
			case "setminrate":
				answer = SetMinRate(update)
			case "getquotes":
				// answer = GetQuote()
				largeAnswer = GetQuotes(update)
				large = true
			case "automod":
				answer = AutoOnOff(update)
			case "help":
				answer = GetHelpInfo()
			case "setautotimeout":
				answer = SetAutoTimeout(update)
			}

		}

		if !large {
			msg := tgbotapi.NewMessage(chatID, html.UnescapeString(answer))
			msg.ReplyMarkup = keyboard
			bot.Send(msg)
		} else {
			for _, v := range largeAnswer {
				msg := tgbotapi.NewMessage(chatID, html.UnescapeString(v))
				msg.ReplyMarkup = keyboard
				bot.Send(msg)
			}
		}
	}
}
