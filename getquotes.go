package main

import (
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"golang.org/x/net/html/charset"
	"gopkg.in/telegram-bot-api.v4"
)

func GetQuotes(upd tgbotapi.Update) []string {

	b, ok := dwnldRandom()
	if !ok {
		return []string{"Что-то пошло не так, не могу обратиться к башу:("}
	}

	quotes := parseQuotes(b)
	ending := make([]string, 0)

	if upd.UpdateID != -1 {
		chatID := upd.Message.Chat.ID
		u := chats.GetUser(chatID)
		for _, v := range quotes {
			if len(ending) >= u.MaxQuotes {
				break
			}
			if v.Rate >= u.MinRate {
				ending = append(ending, v.String())
			}
		}
	} else {
		for _, v := range quotes {
			ending = append(ending, v.String())
		}
	}
	return ending
}

func dwnldRandom() ([]byte, bool) {
	res, err := http.Get("http://bash.im/random")
	if err != nil {
		return nil, false
	}
	defer res.Body.Close()
	utf8, err := charset.NewReader(res.Body, res.Header.Get("Content-Type"))
	if err != nil {
		return nil, false
	}
	b, _ := ioutil.ReadAll(utf8)
	return b, true
}

func parseQuotes(b []byte) []Quote {
	b1, b2 := GetB(b)
	if b1 == -1 {
		return nil
	}

	quotes := make([]Quote, 0)
	for b1 != -1 && b2 != -1 {

		q := Quote{}

		rateb := MyIndex(string(b), "class=\"rating\"")
		ratee := MyIndex(string(b[rateb:]), "</span>")
		if rateb == -1 || ratee == -1 {
			b = b[b1+b2:]
			b1, b2 = GetB(b)
			continue
		}
		rate := string(b[rateb+15 : rateb+ratee])
		rn, err := strconv.Atoi(rate)
		if err != nil {
			rn = 0
		}
		q.Rate = rn

		numb := MyIndex(string(b), "/quote/")
		nume := MyIndex(string(b[numb:]), "/rulez")
		if numb == -1 || nume == -1 {
			b = b[b1+b2:]
			b1, b2 = GetB(b)
			continue
		}
		nums := string(b[numb+7 : numb+nume])
		num, err := strconv.Atoi(nums)
		if err != nil {
			num = 0
		}
		q.Number = num

		textb := MyIndex(string(b), "class=\"text\"")
		texte := MyIndex(string(b[textb:]), "</div>")
		if textb == -1 || texte == -1 {
			b = b[b1+b2:]
			b1, b2 = GetB(b)
			continue
		}
		text := string(b[textb+13 : textb+texte])

		t := strings.Replace(text, "<br>", "\n", -1)
		t = strings.Replace(t, "<br />", "\n", -1)
		t = strings.Replace(t, "<br/>", "\n", -1)
		q.Text = t
		quotes = append(quotes, q)

		b = b[b1+b2:]
		b1, b2 = GetB(b)
	}
	return quotes
}

func GetB(b []byte) (int, int) {
	b1 := MyIndex(string(b), "class=\"actions\"")
	b2 := MyIndex(string(b[b1+1:]), "class=\"actions\"")
	if b2 == -1 {
		b2 = MyIndex(string(b[b1+1:]), "<div class=\"quote more\"")
	}
	return b1, b2
}

func MyIndex(s, sep string) int {
	for i := range s {
		if i+len(sep) > len(s) {
			return -1
		}
		if s[i:i+len(sep)] == sep {
			return len(s[:i])
		}
	}
	return -1
}
