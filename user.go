package main

type User struct {
	ID            int64 `json:"id"`
	AutoUpdate    bool  `json:"autoUpdate"`
	MinRate       int   `json:"minRate"`
	UpdatePerTime int   `json:"updatePerTime"`
	MaxQuotes     int   `json:"maxQuotes"`
}

func NewUser(id int64) *User {
	u := &User{}
	u.ID = id
	u.AutoUpdate = false
	u.MinRate = 5000
	u.UpdatePerTime = 60
	u.MaxQuotes = 5
	return u
}
